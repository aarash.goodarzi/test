//
//  ViewController.swift
//  Test Project
//
//  Created by Arash Goodarzi on 3/2/20.
//  Copyright © 2020 Mehrdad Goodarzi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Vars
    var purpleBtn: SelectionButton?
    var yellowBtn: SelectionButton?
    var redBtn: SelectionButton?
    var circleBtn: CircleButton?
    var circleView: CircleView?
    var views: [UIView] {
        let views = [redBtn,purpleBtn,yellowBtn,circleView,circleBtn].compactMap { $0 }
        return views
    }
    var isExpanded = false {
        didSet {
            view.isUserInteractionEnabled = false
            isExpanded ? expand() : shrink()
        }
    }
  
    
    //MARK: - View Cylcle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareViews()
        
    }
    
    //MARL: - Funcs
    func prepareViews() {
        initilizeViews()
        setConstraints()
    }
    
    //MARK: Animations
    func expand() {
        circleBtn?.playSpinningAnimation(toRight: true)
        moveInTurn(toDown: true)
    }
    
    //**
    func shrink() {
        circleBtn?.playSpinningAnimation(toRight: false)
        moveInTurn(toDown: false)
    }
    
    //**
    func moveInTurn(toDown: Bool) {

        let width: CGFloat = toDown ? 100 : 30
        let maxCenterContraint: CGFloat = 200
        let firstBtn: SelectionButton? = toDown ? yellowBtn : redBtn
        let secondBtn: SelectionButton? = purpleBtn
        let thirdBtn: SelectionButton? = toDown ? redBtn : yellowBtn
        let firstBtnDestY = toDown ? maxCenterContraint : 0
        let secondBtnDestY = toDown ? maxCenterContraint - 40 : 0
        let thirdBtnDestY = toDown ? maxCenterContraint - 80 : 0
        
        if toDown {
            firstBtn?.playMoveAnimation(to: firstBtnDestY, completion: {
                firstBtn?.playResizeAnimation(to: width, completion: nil)
                secondBtn?.playMoveAnimation(to: secondBtnDestY, completion: {
                    secondBtn?.playResizeAnimation(to: width, completion: nil)
                    thirdBtn?.playMoveAnimation(to: thirdBtnDestY, completion: {
                        thirdBtn?.playResizeAnimation(to: width, completion: {
                            self.view.isUserInteractionEnabled = true
                        })
                    })
                })
            })
            return
        }
        
        if !toDown {
            
            firstBtn?.playResizeAnimation(to: width, completion: {
                firstBtn?.playMoveAnimation(to: firstBtnDestY, completion: nil)
                secondBtn?.playResizeAnimation(to: width, completion: {
                    secondBtn?.playMoveAnimation(to: firstBtnDestY, completion: nil)
                    thirdBtn?.playResizeAnimation(to: width, completion: {
                        thirdBtn?.playMoveAnimation(to: firstBtnDestY, completion: {
                            self.view.isUserInteractionEnabled = true
                        })
                    })
                })
            })
        }
    }
    
    
    //MARK: Preparing Views
    func initilizeViews() {
        redBtn = SelectionButton(title: "Story", color: .red)
        purpleBtn = SelectionButton(title: "Activity",color: .purple)
        yellowBtn = SelectionButton(title: "Place",color: .yellow)
        circleBtn = CircleButton(color: .white)
        circleBtn?.setImage(UIImage(named: "x.pdf"), for: .normal)
        circleBtn?.addTarget(self, action: #selector(change), for: .touchUpInside)
        circleBtn?.isUserInteractionEnabled = true
        circleView = CircleView(color: .white)
        views.forEach { self.view.addSubview($0)}
        
    }
    
    //**
    @objc
    func change() {
        isExpanded = !isExpanded
    }
    
    //**
    func setConstraints() {
        circleView?.height(100).width(100).center()
        circleBtn?.height(50).width(50).center()
        redBtn?.height(30).width(30).center()
        purpleBtn?.height(30).width(30).center()
        yellowBtn?.height(30).width(30).center()
    }
    
    //end of class
}

