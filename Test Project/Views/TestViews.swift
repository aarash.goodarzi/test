//
//  TestView.swift
//  Test Project
//
//  Created by Arash Goodarzi on 3/2/20.
//  Copyright © 2020 Mehrdad Goodarzi. All rights reserved.
//

import UIKit

class CircleView: UIView {
    
    convenience init(color: UIColor) {
        self.init()
        backgroundColor = color
        DispatchQueue.main.async {
            self.roundAllCorners()
            self.layer.opacity = 0.3
        }
    }
    
}
