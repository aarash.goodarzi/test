//
//  TestButtom.swift
//  Test Project
//
//  Created by Arash Goodarzi on 3/2/20.
//  Copyright © 2020 Mehrdad Goodarzi. All rights reserved.
//

import UIKit

class SelectionButton: UIButton, Movable, Resizeble {
    
    convenience init(title: String, color: UIColor) {
        self.init()
        setTitle(title, for: .normal)
        setTitleColor(.white, for: .normal)
        backgroundColor = color
        DispatchQueue.main.async {
            self.roundAllCorners()
        }
    }
}

//***
class CircleButton: UIButton, Spinnable {
    
    convenience init(color: UIColor) {
        self.init()
        backgroundColor = color
        DispatchQueue.main.async {
            self.roundAllCorners()
        }
    }
    
}

