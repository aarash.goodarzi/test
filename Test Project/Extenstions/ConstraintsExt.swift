//
//  Constraint.swift
//  Test Project
//
//  Created by Arash Goodarzi on 3/3/20.
//  Copyright © 2020 Mehrdad Goodarzi. All rights reserved.
//

import UIKit


extension UIView {
    
    //MARK: - Height and Width
    @discardableResult
    func width(_ width: CGFloat) -> UIView {
        prepareForConstraint(view: nil)
        superview?.addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width))
        updateConstraints(for: self)
        return self
    }
    
    //**
    @discardableResult
    func height(_ height: CGFloat) -> UIView {
        prepareForConstraint(view: nil)
        superview?.addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height))
        updateConstraints(for: self)
        return self
    }
    
    
    //Mark: - Constraints
    func prepareForConstraint(view: UIView?) {
        self.translatesAutoresizingMaskIntoConstraints = false
        view?.translatesAutoresizingMaskIntoConstraints = false
    }
    
    //**
    func updateConstraints(for view: UIView) {
        view.setNeedsUpdateConstraints()
        view.updateConstraintsIfNeeded()
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
    
    
    //**
    @discardableResult
    func center(_ offset: CGFloat = 0, of view: UIView? = nil) -> UIView {
        centerHorizontally(offset, of: view)
        centerVertically(offset, of: view)
        return self
    }
    
    //**
    @discardableResult
    func centerHorizontally(_ offset: CGFloat = 0, of view: UIView? = nil ) -> UIView {
        
        let view: UIView? = view != nil ? view : superview
        prepareForConstraint(view: view)
        self.superview?.addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: offset))
        updateConstraints(for: self)
        return self
    }
    
    //**
    @discardableResult
    func centerVertically(_ offset: CGFloat = 0, of view: UIView? = nil ) -> UIView{
        
        let view: UIView? = view != nil ? view : superview
        prepareForConstraint(view: view)
        self.superview?.addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: offset))
        updateConstraints(for: self)
        return self
    }
    
    //**
    var widthConstraint: NSLayoutConstraint? {
        
        get {
            return superview?.constraints.first(where: {
                $0.firstItem === self && $0.firstAttribute == .width
            })
        }
        set { setNeedsLayout() }
    }
    
    //**
    var centerYConstraint: NSLayoutConstraint? {
        
        get {
            return superview?.constraints.first(where: {
                $0.firstItem === self && $0.firstAttribute == .centerY
            })
        }
        set { setNeedsLayout() }
    }
    
}


