//
//  AnimationExtensions.swift
//  Test Project
//
//  Created by Arash Goodarzi on 3/3/20.
//  Copyright © 2020 Mehrdad Goodarzi. All rights reserved.
//

import UIKit

protocol Movable: UIView {
    
}

extension Movable {
    
    func playMoveAnimation(to destinationY: CGFloat, interval: TimeInterval = 0.2, completion: (() -> Void)?) {
        
        UIView.animate(withDuration: interval, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: [], animations: {

            self.prepareForConstraint(view: self.superview)
            self.centerYConstraint?.constant = destinationY
            self.updateConstraints(for: self)
            self.superview?.layoutIfNeeded()
        }) { _ in
            completion?()
            
        }
    }
}

//***
protocol Resizeble: UIView {
    
}

extension Resizeble {
   
    func playResizeAnimation(to width: CGFloat, interval: TimeInterval = 0.2, completion: (() -> Void)?) {
        
        UIView.animate(withDuration: interval, animations: {
            
            self.prepareForConstraint(view: self.superview)
            self.widthConstraint?.constant = width
            self.updateConstraints(for: self)
            self.superview?.layoutIfNeeded()
        }) { _ in
            completion?()
            
        }
    }
}

//***
protocol Spinnable: UIView {
    
}

extension Spinnable {
   
    func playSpinningAnimation(toRight: Bool, interval: TimeInterval = 0.4) {
        
        
        let rotationAngle: CGFloat = CGFloat(Double.pi / 4)
        let transform = toRight ? CGAffineTransform(rotationAngle: rotationAngle) : .identity
        UIView.animate(withDuration: interval, animations: {
            self.transform = transform
            //self.layoutIfNeeded()
        })
    }
}

