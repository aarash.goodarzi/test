//
//  Extensions.swift
//  Test Project
//
//  Created by Arash Goodarzi on 3/2/20.
//  Copyright © 2020 Mehrdad Goodarzi. All rights reserved.
//

import UIKit

extension UIView {
    func roundAllCorners() {
        let radius = frame.height / 2
        self.layer.cornerRadius = radius
        clipsToBounds = true
    }

}
